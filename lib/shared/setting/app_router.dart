import 'package:emed/screen/auth/auth_screen.dart';
import 'package:emed/screen/home/home_screen.dart';
import 'package:flutter/material.dart';

const String ROUTER_SPLASH = "/";
const String ROUTER_AUTH = "auth";
const String ROUTER_HOME = "home";

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    Map<String, dynamic> arguments = settings.arguments;
    switch (settings.name) {
      case ROUTER_AUTH:
        return _getPageRoute(
          routeName: settings.name,
          viewToShow: AuthScreen(),
        );
      case ROUTER_HOME:
        return _getPageRoute(
          routeName: settings.name,
          viewToShow: HomeScreen(),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(child: Text('No route defined for ${settings.name}')),
          ),
        );
    }
  }

  static PageRoute _getPageRoute({String routeName, Widget viewToShow}) {
    return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (_) => viewToShow,
    );
  }
}
