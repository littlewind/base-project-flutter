import 'package:emed/data/repository/auth_repository_impl.dart';
import 'package:get_it/get_it.dart';

import 'repository/auth_repository.dart';

GetIt serviceLocator = GetIt.instance;

void setupServiceLocator() {
  serviceLocator.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl());
}
