import 'package:emed/provider/model/app_setting_model.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class AppProvider {
  factory AppProvider() {
    if (_instance == null) {
      _instance = AppProvider._getInstance();
    }
    return _instance;
  }

  static AppProvider _instance;
  AppProvider._getInstance();

  // Declare list of model here
  AppSettingModel appSettingModel;

  init() {
    appSettingModel = AppSettingModel();
  }

  List<SingleChildWidget> get providers => [
    ChangeNotifierProvider(create: (_) => appSettingModel),
  ];

}