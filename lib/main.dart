import 'package:emed/provider/app_providers.dart';
import 'package:emed/shared/setting/app_router.dart';
import 'package:emed/shared/setting/constant.dart';
import 'package:emed/shared/setting/navigation_services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'data/service_locator.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupServiceLocator();
  await AppProvider().init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: AppProvider().providers,
      child: MaterialApp(
        title: 'Emed',
        theme: appThemeData,
        navigatorKey: NavigationService().navigationKey,
        onGenerateRoute: AppRouter.generateRoute,
        initialRoute: ROUTER_AUTH,
      ),
    );
  }
}
