import 'package:emed/provider/model/app_setting_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    context.read<AppSettingModel>().getAppSetting();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Consumer<AppSettingModel>(
          builder: (_, model, __) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('User ID: ${model.appSetting?.userId ?? ''}'),
                Text('Username: ${model.appSetting?.userName ?? ''}'),
              ],
            );
          },
        ),
      ),
    );
  }
}
