import 'package:emed/shared/setting/app_router.dart';
import 'package:emed/shared/setting/navigation_services.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'auth_screen_logic.dart';

class AuthScreen extends StatefulWidget {
  AuthScreen({Key key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  AuthScreenLogic logic = AuthScreenLogic();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          child: Text('Login'),
          onPressed: () {
            logic.loginWithEmailAndPass(
              email: 'email',
              password: 'pass',
              successHandler: _logInSuccess,
            );
          },
        ),
      ),
    );
  }

  _logInSuccess() {
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString('userId', 'hust-vin-UID');
      prefs.setString('userName', 'E-med Username');
    });
    NavigationService().pushReplacementNamed(ROUTER_HOME);
  }
}